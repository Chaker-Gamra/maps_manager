module gitlab.com/Chaker-Gamra/maps_manager

// +heroku goVersion go1.14
go 1.14

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-openapi/spec v0.19.15 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.3.0
	github.com/swaggo/http-swagger v0.0.0-20200308142732-58ac5e232fba
	github.com/swaggo/swag v1.6.9
	github.com/tidwall/gjson v1.6.3
	github.com/tidwall/match v1.0.2 // indirect
	golang.org/x/tools v0.0.0-20201202100533-7534955ac86b // indirect
)
