package models

import (
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/tidwall/gjson"
	polyline "gitlab.com/Chaker-Gamra/maps_manager/flexpolyline"
)

type Provider interface {
	CalculateRoute(origin string, destination string, traffic bool, withPath bool) (DirectionOutput, error)
	CalculateMatrix(origins []string, destinations []string, traffic bool) (DistanceMatrixOutput, error)
}

type Google struct {
}

func (g Google) CalculateRoute(origin string, destination string, traffic bool, withPath bool) (DirectionOutput, error) {
	return DirectionOutput{}, NewError("Your API key is not Valid")
}
func (g Google) CalculateMatrix(origins []string, destinations []string, traffic bool) (DistanceMatrixOutput, error) {
	return DistanceMatrixOutput{}, NewError("Your API key is not Valid")
}

type Here struct {
}

func (h Here) CalculateRoute(origin string, destination string, traffic bool, withPath bool) (DirectionOutput, error) {

	//Construct URL for the API
	URL := "https://router.hereapi.com/v8/routes?transportMode=car"
	URL += "&origin=" + origin + "&destination=" + destination + "&return=travelSummary"
	if withPath {
		URL += ",polyline"
	}
	URL += "&apiKey=-qvlKiwaSI0kar9b6J5ub5TYVQAu0tT7C62i8xb_vuI"

	//Send HTTP Request to get result from Here Maps API
	resp, err := http.Get(URL)
	if err != nil {
		//Error While Getting Response from Here Maps API
		return DirectionOutput{}, NewError(err.Error())
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		//Error While Reading Response
		return DirectionOutput{}, NewError(err.Error())
	}
	//change data to a json format
	data := string(body)

	//In Case of Route calculation failed:
	notices := gjson.Get(data, "notices")
	if notices.Exists() {
		errorMessage := gjson.Get(data, "notices.0.title")
		return DirectionOutput{}, NewError(errorMessage.String())
	}

	//In Case of Everything went Right

	duration := gjson.Get(data, "routes.0.sections.0.travelSummary.baseDuration").Float()
	durationInTraffic := duration
	if traffic {
		durationInTraffic = gjson.Get(data, "routes.0.sections.0.travelSummary.duration").Float()
	}
	distance := gjson.Get(data, "routes.0.sections.0.travelSummary.length").Float()

	//we search for path when withPath option true
	var path []Location
	if withPath {
		polylineString := gjson.Get(data, "routes.0.sections.0.polyline").String()
		polylineObject, _ := polyline.Decode(polylineString)
		for _, point := range polylineObject.Coordinates() {
			path = append(path, Location{point.Lat, point.Lng})
		}
	}

	return DirectionOutput{duration, durationInTraffic, distance, path}, nil

}
func (h Here) CalculateMatrix(origins []string, destinations []string, traffic bool) (DistanceMatrixOutput, error) {
	//Construct URL for the API
	URL := "https://matrix.route.ls.hereapi.com/routing/7.2/calculatematrix.json?apiKey=-qvlKiwaSI0kar9b6J5ub5TYVQAu0tT7C62i8xb_vuI"
	URL += "&summaryAttributes=tt,di"
	for index, origin := range origins {
		URL += "&start" + strconv.FormatInt(int64(index), 10) + "=" + origin
	}
	for index, destination := range destinations {
		URL += "&destination" + strconv.FormatInt(int64(index), 10) + "=" + destination
	}

	URL += "&mode=fastest;car"

	if traffic {
		URL += ";traffic:enabled&departure=now"
	} else {
		URL += ";traffic:disabled"
	}
	//Send HTTP Request to get result from Here Maps API
	resp, err := http.Get(URL)
	if err != nil {
		//Error While Getting Response from Here Maps API
		return DistanceMatrixOutput{}, NewError(err.Error())
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		//Error While Reading Response
		return DistanceMatrixOutput{}, NewError(err.Error())
	}
	//change data to a json format
	data := string(body)

	//In Case of Route calculation failed:
	matrixEntry := gjson.Get(data, "response.matrixEntry")
	if matrixEntry.Exists() {

		directionsArray := make([][]DistanceMatrixEntity, len(destinations))
		for i := range directionsArray {
			directionsArray[i] = make([]DistanceMatrixEntity, len(origins))
		}

		matrixEntry.ForEach(func(key, value gjson.Result) bool {
			row := gjson.Get(value.String(), "startIndex").Int()
			col := gjson.Get(value.String(), "destinationIndex").Int()

			directionsArray[row][col].Distance = gjson.Get(value.String(), "summary.distance").Float()
			directionsArray[row][col].Duration = gjson.Get(value.String(), "summary.travelTime").Float()
			directionsArray[row][col].DurationInTraffic = gjson.Get(value.String(), "summary.travelTime").Float()
			return true // keep iterating
		})
		return DistanceMatrixOutput{directionsArray}, nil
	} else {
		return DistanceMatrixOutput{}, NewError("Failed To Get Distance Matrix")
	}

}
