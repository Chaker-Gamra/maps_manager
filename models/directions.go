package models

type Location struct {
	Lat float64 `json:"lat" example:"34.1330949"`
	Lng float64 `json:"lng" example:"-117.9143879"`
}

type DirectionOutput struct {
	Duration          float64    `json:"duration" example:"3031"`
	DurationInTraffic float64    `json:"durationInTraffic" example:"3062"`
	Distance          float64    `json:"distance" example:"57824"`
	Path              []Location `json:"path"`
}
