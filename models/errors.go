package models

import (
	"encoding/json"
	"net/http"
)

// customError is a trivial implementation of error.
type CustomError struct {
	Msg string `json:"error" example:"Provider is missing"`
}

func (e *CustomError) Error() string {
	return e.Msg
}

// New returns an error that formats as the given text.
func NewError(text string) error {
	return &CustomError{text}
}

func JSONError(w http.ResponseWriter, err *CustomError, code int) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.WriteHeader(code)
	json.NewEncoder(w).Encode(*err)
}
