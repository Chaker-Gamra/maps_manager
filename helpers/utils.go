package helpers

import (
	"net/url"
	"strconv"
	"strings"

	"gitlab.com/Chaker-Gamra/maps_manager/models"
)

func CheckProvider(params url.Values) (string, error) {

	if provider, ok := params["provider"]; ok {
		if len(provider) != 1 {
			return "", models.NewError("Provider is a single value (google or here)")
		} else {
			if provider[0] == "google" || provider[0] == "here" {
				return provider[0], nil
			} else {
				return "", models.NewError("Provider can only be google or here")
			}
		}
	}
	return "", models.NewError("Provider is missing")

}

func CheckOrigin(params url.Values) (string, error) {
	if origin, ok := params["origin"]; ok {
		if len(origin) != 1 {
			return "", models.NewError("Origin is a single value")
		} else {
			if LatLngFormat(origin[0]) {
				return origin[0], nil
			} else {
				return "", models.NewError("Origin needs to be in this format lat,lng")
			}
		}

	}

	return "", models.NewError("An Origin is missing")

}

func CheckDestination(params url.Values) (string, error) {
	if destination, ok := params["destination"]; ok {
		if len(destination) != 1 {
			return "", models.NewError("Destination is a single value")
		} else {
			if LatLngFormat(destination[0]) {
				return destination[0], nil
			} else {
				return "", models.NewError("Destination needs to be in this format lat,lng")
			}
		}
	}

	return "", models.NewError("A destination is missing")

}

func LatLngFormat(s string) bool {
	chunks := strings.Split(s, ",")
	if len(chunks) != 2 {
		return false
	} else {
		if !isNumeric(chunks[0]) || !isNumeric(chunks[1]) {
			return false
		}
	}

	return true
}

func isNumeric(s string) bool {
	_, err := strconv.ParseFloat(s, 64)
	return err == nil
}

func GetTraffic(params url.Values) (bool, error) {

	if traffic, ok := params["traffic"]; ok {
		if traffic[0] == "true" {
			return true, nil
		} else if traffic[0] == "false" {
			return false, nil
		}
		return false, models.NewError("The traffic option needs to be true or false")
	}
	return false, nil
}

func GetWithPath(params url.Values) (bool, error) {

	if withPath, ok := params["withPath"]; ok {
		if withPath[0] == "true" {
			return true, nil
		} else if withPath[0] == "false" {
			return false, nil
		}
		return false, models.NewError("withPath option needs to be true or false")

	}
	return false, nil
}

func CheckOrigins(params url.Values) ([]string, error) {
	if origins, ok := params["origins"]; ok {

		chunks := strings.Split(origins[0], "|")
		if len(chunks) == 0 {
			return nil, models.NewError("You Need at Least one origin")
		} else {
			for _, val := range chunks {
				if !LatLngFormat(val) {
					return nil, models.NewError("The format of your origins is not valid")
				}
			}
		}
		return chunks, nil
	}

	return nil, models.NewError("List of Origins is missing")

}

func CheckDestinations(params url.Values) ([]string, error) {
	if destinations, ok := params["destinations"]; ok {

		chunks := strings.Split(destinations[0], "|")
		if len(chunks) == 0 {
			return nil, models.NewError("You Need at Least one destination")
		} else {
			for _, val := range chunks {
				if !LatLngFormat(val) {
					return nil, models.NewError("The format of your destinations is not valid")
				}
			}
		}
		return chunks, nil
	}

	return nil, models.NewError("List of Destinations is missing")
}
