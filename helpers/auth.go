package helpers

import (
	"fmt"
	"net/http"
	"os"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"gitlab.com/Chaker-Gamra/maps_manager/models"
)

func GenerateJWT(durationInMinutes time.Duration) (string, error) {

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"authorized": true,
		"user":       "yuso",
		"exp":        time.Now().Add(time.Minute * durationInMinutes).Unix(),
	})

	secretKey := os.Getenv("SecretKey")
	tokenString, err := token.SignedString([]byte(secretKey))
	if err != nil {
		fmt.Println("Something Went Wrong!!")
		return "", err
	}

	return tokenString, err
}

//Authentication Middleware to protect routes
func IsAuthenticated(endPoint func(w http.ResponseWriter, r *http.Request)) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Header["Token"] != nil {
			token, err := jwt.Parse(r.Header.Get("Token"), func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("Token has a wrong signing method")
				}
				secretKey := os.Getenv("SecretKey")
				return []byte(secretKey), nil
			})
			if err != nil {
				models.JSONError(w, &models.CustomError{Msg: err.Error()}, 401)
				return
			}
			if token.Valid {
				endPoint(w, r)
			} else {
				models.JSONError(w, &models.CustomError{Msg: "Token key is Not Valid"}, 401)
			}
		} else {
			models.JSONError(w, &models.CustomError{Msg: "No Token key Provided in the request's header"}, 401)
		}

	})
}
