package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/Chaker-Gamra/maps_manager/helpers"
	"gitlab.com/Chaker-Gamra/maps_manager/models"
)

// GetDirections godoc
// @Summary Calculate Route
// @Description Our First API To Get Direction info between two locations
// @Tags direction API
// @Accept  json
// @Produce  json
// @Param provider query string false "Select A Provider" Enums(Google, Here)
// @Param origin query string false "Put An Origin in this format lat,lng"
// @Param destination query string false "Put A Destination in this format lat,lng"
// @Param withPath query string false "WithPath Option" Enums(True, False)
// @Param traffic query string false "Traffic Option" Enums(True, False)
// @Success 200 {object} models.DirectionOutput
// @Failure 400 {object} models.CustomError
// @Security ApiKeyAuth
// @Router /directions [get]
func CalculateRoute(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("directions API")
	w.Header().Set("Content-Type", "application/json")

	//Authorization
	/*reqToken := r.Header.Get("Authorization")
	splitToken := strings.Split(reqToken, "Bearer ")
	reqToken = splitToken[1]
	fmt.Printf(reqToken)*/

	//Read Query Parameters
	params := r.URL.Query()
	providerName, err := helpers.CheckProvider(params)
	if err != nil {
		models.JSONError(w, err.(*(models.CustomError)), 400)
		return
	}
	origin, err := helpers.CheckOrigin(params)
	if err != nil {
		models.JSONError(w, err.(*(models.CustomError)), 400)
		return
	}
	destination, err := helpers.CheckDestination(params)
	if err != nil {
		models.JSONError(w, err.(*(models.CustomError)), 400)
		return
	}
	traffic, err := helpers.GetTraffic(params)
	if err != nil {
		models.JSONError(w, err.(*(models.CustomError)), 400)
		return
	}
	withPath, err := helpers.GetWithPath(params)
	if err != nil {
		models.JSONError(w, err.(*(models.CustomError)), 400)
		return
	}
	var provider models.Provider

	if providerName == "google" {
		provider = models.Google{}
	} else {
		provider = models.Here{}
	}

	result, err := provider.CalculateRoute(origin, destination, traffic, withPath)

	if err != nil {
		models.JSONError(w, err.(*(models.CustomError)), 400)
		return
	}

	json.NewEncoder(w).Encode(result)
}
