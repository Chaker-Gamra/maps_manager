package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/Chaker-Gamra/maps_manager/helpers"
	"gitlab.com/Chaker-Gamra/maps_manager/models"
)

//  Get Distance matrix godoc
// @Summary Calculate Matrix
// @Description Get a 2D array of directions
// @Tags Distance Matrix API
// @Accept  json
// @Produce  json
// @Success 200
// @Param provider query string false "Select A Provider" Enums(Google, Here)
// @Param origins query string false "Put athe list of Origins in this format lat,lng|lat,lng ..."
// @Param destinations query string false "Put the list of Destinations in this format lat,lng|lat,lng ..."
// @Param traffic query string false "Traffic Option" Enums(True, False)
// @Success 200 {object} models.DistanceMatrixOutput
// @Security ApiKeyAuth
// @Router /distance_matrix [get]
func CalculateMatrix(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("distance matrix API")

	w.Header().Set("Content-Type", "application/json")

	//Authorization
	/*reqToken := r.Header.Get("Authorization")
	splitToken := strings.Split(reqToken, "Bearer ")
	reqToken = splitToken[1]
	fmt.Printf(reqToken)*/

	//Read Query Parameters
	params := r.URL.Query()
	providerName, err := helpers.CheckProvider(params)
	if err != nil {
		models.JSONError(w, err.(*(models.CustomError)), 400)
		return
	}
	origins, err := helpers.CheckOrigins(params)
	if err != nil {
		models.JSONError(w, err.(*(models.CustomError)), 400)
		return
	}
	destinations, err := helpers.CheckDestinations(params)
	if err != nil {
		models.JSONError(w, err.(*(models.CustomError)), 400)
		return
	}
	traffic, err := helpers.GetTraffic(params)
	if err != nil {
		models.JSONError(w, err.(*(models.CustomError)), 400)
		return
	}

	var provider models.Provider

	if providerName == "google" {
		provider = models.Google{}
	} else {
		provider = models.Here{}
	}

	result, err := provider.CalculateMatrix(origins, destinations, traffic)

	if err != nil {
		models.JSONError(w, err.(*(models.CustomError)), 400)
		return
	}

	json.NewEncoder(w).Encode(result)
}
