package router

import (
	"github.com/gorilla/mux"
	"gitlab.com/Chaker-Gamra/maps_manager/controllers"
	"gitlab.com/Chaker-Gamra/maps_manager/helpers"
)

func InitRouter() *mux.Router {

	//Create a new mux router
	r := mux.NewRouter().StrictSlash(true)

	r.Handle("/directions", helpers.IsAuthenticated(controllers.CalculateRoute)).Methods("GET")       //GET / directions
	r.Handle("/distance_matrix", helpers.IsAuthenticated(controllers.CalculateMatrix)).Methods("GET") //GET / distance_matrix
	//Serve static files
	//sh := http.StripPrefix("/", http.FileServer(http.Dir("./static/")))
	//r.PathPrefix("/").Handler(sh)

	return r
}
